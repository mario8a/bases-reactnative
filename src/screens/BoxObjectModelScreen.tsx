import React from 'react'
import { Text, View, StyleSheet } from 'react-native';

export const BoxObjectModelScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Box Object model</Text>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'red'
    },
    title: {
      paddingHorizontal: 100,
      paddingVertical: 100,
      fontSize: 20,
      // margin: 10,
      marginVertical: 45,
      marginHorizontal: 50,
      // width: 200,
      borderWidth: 10
    }
});