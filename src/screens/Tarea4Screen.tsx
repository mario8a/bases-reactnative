import React from 'react'
import { StyleSheet, View } from 'react-native';

export const Tarea4Screen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.cajaMorada} />
      <View style={styles.cajaNaranja} />
      <View style={styles.cajaAzul} />
    </View>
  )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#28425B',
      flexDirection: 'row-reverse',
      justifyContent: 'space-between',
      paddingVertical: 50
    },
    cajaMorada: {
      width: 100,
      height: 100,
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#5856d6',
      // alignSelf: 'flex-end' // Si no tuviera el row reverse
    },
    cajaNaranja: {
      alignSelf: 'center',
      width: 100,
      height: 100,
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#ff9409'
      // alignSelf: 'center' // Si no tuviera el row reverse
    },
    cajaAzul: {
      alignSelf: 'flex-end',
      width: 100,
      height: 100,
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#09f7ff'
      // alignSelf: 'center' // Si no tuviera el row reverse
    },
});