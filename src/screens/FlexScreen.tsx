import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export const FlexScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.caja1}>Caja 1</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
      <Text style={styles.caja2}>Caja 2</Text>
      <Text style={styles.caja3}>Caja 3</Text>
    </View>
  )
}

//alignItems
//stretch -> default
//baseline -> Los hijos ocupan unicamente el espacio minimo disponible


// alignSelf -> Sirve para que un hijo pueda sobrescribir algun comportamiento que el padre le esta diciendo a el
//flex-wrap -> indica que haran los hijos cuando son mas grandes que el padre

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#28c4d9',
    // flexDirection: 'row',
    // justifyContent: 'space-around' // En meido del componente padre,
    // justifyContent: 'center',
    // alignItems: 'center'

    //Ejercicio-> Hacer que las cajas aparezcan abajo a la derecha en forma de row
    // flexDirection: 'row',
    // alignItems: 'flex-end',
    // justifyContent: 'flex-end'

    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap'



  },
  caja1: {
    // flex: 4, // 40% de la pantalla
    borderWidth: 2,
    borderColor: 'white',
    fontSize: 30,
    // alignSelf: 'flex-start',
    // alignSelf: 'center',

  },
  caja2: {
    // flex: 4, // 40
    borderWidth: 2,
    borderColor: 'white',
    fontSize: 30
  },
  caja3: {
    // flex: 2 , // 20 de la pantalla
    borderWidth: 2,
    borderColor: 'white',
    fontSize: 30,
    // alignSelf: 'flex-end',
  }
});