import React from 'react'
import { StyleSheet, View } from 'react-native';

export const Tarea6Screen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.cajaMorada} />
      <View style={styles.cajaNaranja} />
      <View style={styles.cajaAzul} />
    </View>
  )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#28425B',
      flexDirection: 'column',
      paddingVertical: 25
    },
    cajaMorada: {
      flex: 1,
      width: 'auto',
      height: 100,
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#5856d6'
    },
    cajaNaranja: {
      flex: 2,
      width: 'auto',
      height: 100,
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#ff9409'
    },
    cajaAzul: {
      flex: 3,
      width: 'auto',
      height: 100,
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#09f7ff'
    },
});