import React from 'react'
import { StyleSheet, View } from 'react-native';

export const Tarea5Screen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.cajaMorada} />
      <View style={styles.cajaNaranja} />
      <View style={styles.cajaAzul} />
    </View>
  )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#28425B',
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 25 
    },
    cajaMorada: {
      width: 100,
      // height: 'auto', // Se puede quitar ya que por defecto se estiran lo max
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#5856d6'
    },
    cajaNaranja: {
      width: 100,
      // height: 'auto', // Se puede quitar ya que por defecto se estiran lo max
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#ff9409'
    },
    cajaAzul: {
      width: 100,
      // height: 'auto', // Se puede quitar ya que por defecto se estiran lo max
      borderWidth: 10,
      borderColor: 'white',
      backgroundColor: '#09f7ff'
    },
});