import React from 'react'
import { SafeAreaView, Text, View } from 'react-native';
import { BoxObjectModelScreen } from './src/screens/BoxObjectModelScreen';
import { ContadorScreen } from './src/screens/ContadorScreen';
import { DimensionesScreen } from './src/screens/DimensionesScreen';
import { FlexScreen } from './src/screens/FlexScreen';
import { HolaMundoScreen } from './src/screens/HolaMundoScreen';
import { PositionScreen } from './src/screens/PositionScreen';
import { Tarea2Screen } from './src/screens/Tarea2Screen';
import { Tarea1Screen } from './src/screens/TareaScreen';
import { Tarea3Screen } from './src/screens/Tarea3Screen';
import { Tarea4Screen } from './src/screens/Tarea4Screen';
import { Tarea5Screen } from './src/screens/Tarea5Screen';
import { Tarea6Screen } from './src/screens/Tarea6Screen';
import { Tarea7Screen } from './src/screens/Tarea7Screen';
import { Tarea8Screen } from './src/screens/Tarea8Screen';
import { Tarea9Screen } from './src/screens/Tarea9Screen';
import { Tarea10Screen } from './src/screens/Tarea10Screen';

const App = () => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#28425B'}}>
      {/* <HolaMundoScreen /> */}
      <ContadorScreen />
      {/* <BoxObjectModelScreen /> */}
      {/* <DimensionesScreen /> */}
      {/* <PositionScreen /> */}
      {/* <FlexScreen /> */}
      {/* <Tarea1Screen /> */}
      {/* <Tarea2Screen /> */}
      {/* <Tarea3Screen /> */}
      {/* <Tarea4Screen /> */}
      {/* <Tarea5Screen /> */}
      {/* <Tarea6Screen /> */}
      {/* <Tarea7Screen /> */}
      {/* <Tarea8Screen /> */}
      {/* <Tarea9Screen /> */}
      {/* <Tarea10Screen /> */}
    </SafeAreaView>
  )
}

// SafeAreaView -> Asegura que se vea el view completo sin que lo tape el notch del celular
export default App;